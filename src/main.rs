fn main() {
    println!("Hello, world!");
}

enum Player {
    FIRST,
    SECOND,
}

enum State {
    SCORE(i8, i8),
    DEUCE,
    ADVANTAGE(Player),
    WIN(Player),
}

struct Game {
    player_1_name: String,
    player_2_name: String,
    state: State,
}

impl Game {
    fn new(player_1_name: &str, player_2_name: &str) -> Self {
        Game {
            player_1_name: String::from(player_1_name),
            player_2_name: String::from(player_2_name),
            state: State::SCORE(0, 0),
        }
    }

    fn player_1_scored(&mut self) {
        self.state = Game::scored(&self.state, Player::FIRST);
    }

    fn scored(state: &State, player: Player) -> State {
        return match (state, player) {
            (State::ADVANTAGE(Player::FIRST), Player::SECOND) => State::DEUCE,
            (State::ADVANTAGE(Player::SECOND), Player::FIRST) => State::DEUCE,
            (State::ADVANTAGE(_), player) => State::WIN(player),
            (State::SCORE(40, _), Player::FIRST) => State::WIN(Player::FIRST),
            (State::SCORE(_, 40), Player::SECOND) => State::WIN(Player::SECOND),
            (State::SCORE(30, 40), Player::FIRST) => State::DEUCE,
            (State::SCORE(40, 30), Player::SECOND) => State::DEUCE,
            (State::SCORE(30, other), Player::FIRST) => State::SCORE(40, *other),
            (State::SCORE(other, 30), Player::SECOND) => State::SCORE(*other, 40),
            (State::SCORE(player_1, player_2), Player::FIRST) => State::SCORE(player_1 + 15, *player_2),
            (State::SCORE(player_1, player_2), Player::SECOND) => State::SCORE(*player_1, player_2 + 15),
            (State::DEUCE, player) => State::ADVANTAGE(player),
            (State::WIN(Player::FIRST), _) => State::WIN(Player::FIRST),
            (State::WIN(Player::SECOND), _) => State::WIN(Player::SECOND),
        };
    }

    fn player_2_scored(&mut self) {
        self.state = Game::scored(&self.state, Player::SECOND);
    }

    fn get_store(&self) -> String {
        return match &self.state {
            State::SCORE(player_1, player_2) => format!("{} : {}", player_1, player_2),
            State::DEUCE => format!("Deuce"),
            State::ADVANTAGE(player) => format!("Advantage {}", self.get_name(&player)),
            State::WIN(player) => format!("{} wins", self.get_name(&player))
        };
    }

    fn get_name(&self, player: &Player) -> &String {
        return match player {
            Player::FIRST => &self.player_1_name,
            Player::SECOND => &self.player_2_name,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_starts_with_zero_points() {
        let game = Game::new("Jim", "John");
        assert_eq!(game.get_store(), "0 : 0");
    }

    #[test]
    fn first_point_is_15() {
        let mut game = Game::new("Jim", "John");
        game.player_1_scored();
        assert_eq!(game.get_store(), "15 : 0");
    }

    #[test]
    fn player_2_first_point_is_15() {
        let mut game = Game::new("Jim", "John");
        game.player_2_scored();
        assert_eq!(game.get_store(), "0 : 15");
    }

    #[test]
    fn third_point_is_40() {
        let mut game = Game::new("Jim", "John");
        game.player_1_scored();
        game.player_1_scored();
        game.player_1_scored();
        assert_eq!(game.get_store(), "40 : 0");
    }

    #[test]
    fn player_2_third_point_is_40() {
        let mut game = Game::new("Jim", "John");
        game.player_2_scored();
        game.player_2_scored();
        game.player_2_scored();
        assert_eq!(game.get_store(), "0 : 40");
    }

    #[test]
    fn ball_and_40_wins() {
        let mut game = Game::new("Jim", "John");
        game.player_1_scored();
        game.player_1_scored();
        game.player_1_scored();
        game.player_1_scored();
        assert_eq!(game.get_store(), "Jim wins");
    }

    #[test]
    fn player_2_ball_and_40_wins() {
        let mut game = Game::new("Jim", "John");
        game.player_2_scored();
        game.player_2_scored();
        game.player_2_scored();
        game.player_2_scored();
        assert_eq!(game.get_store(), "John wins");
    }

    #[test]
    fn first_deuce() {
        let mut game = Game::new("Jim", "John");
        game.player_2_scored();
        game.player_2_scored();
        game.player_2_scored();
        game.player_1_scored();
        game.player_1_scored();
        game.player_1_scored();
        assert_eq!(game.get_store(), "Deuce");
    }

    #[test]
    fn first_player_advantage() {
        let mut game = Game::new("Jim", "John");
        game.player_2_scored();
        game.player_2_scored();
        game.player_2_scored();
        game.player_1_scored();
        game.player_1_scored();
        game.player_1_scored();
        game.player_1_scored();
        assert_eq!(game.get_store(), "Advantage Jim");
    }

    #[test]
    fn second_player_advantage() {
        let mut game = Game::new("Jim", "John");
        game.player_2_scored();
        game.player_2_scored();
        game.player_2_scored();
        game.player_1_scored();
        game.player_1_scored();
        game.player_1_scored();
        game.player_2_scored();
        assert_eq!(game.get_store(), "Advantage John");
    }
}